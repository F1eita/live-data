package com.example.praiseapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SomeViewModel: ViewModel() {

    val currentText : MutableLiveData<String> by lazy{
        MutableLiveData<String>()
    }

}