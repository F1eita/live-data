package com.example.praiseapp

object Constants {
    val NAME = "NAME"
    val SURNAME = "SURNAME"
    val MIDDLE_NAME = "MIDDLE_NAME"
    val AGE = "AGE"
    val HOBBIE = "HOBBIE"

    val ALL_DATA = "ALL_DATA"
}