package com.example.praiseapp

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import kotlinx.android.synthetic.main.activity_some.*

class SomeActivity : AppCompatActivity() {

    lateinit var viewModel: SomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_some)

        viewModel = ViewModelProvider(this).get(SomeViewModel::class.java)

        viewModel.currentText.observe(this, Observer {
            someTextView.text = it
        })

        incrementText()
    }

    private fun incrementText(){
        someButton.setOnClickListener{
            viewModel.currentText.value = someEditText.getText().toString()
        }
    }
}