package com.example.praiseapp

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            if (name.getText().toString() == "" || surname.getText().toString() == ""
                || age.getText().toString() == ""){
                textView.setText(R.string.errorMessage)
            }
            else {
                textView.setText("")

                intent = Intent(this, InfoActivity::class.java)

                intent.putExtra(Constants.NAME, name.getText().toString())
                intent.putExtra(Constants.SURNAME, surname.getText().toString())
                intent.putExtra(Constants.MIDDLE_NAME, middleName.getText().toString())
                intent.putExtra(Constants.AGE, age.getText().toString())
                intent.putExtra(Constants.HOBBIE, hobbie.getText().toString())

                startActivity(intent)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.changeBackground -> changeBackground(random())
            R.id.saveData -> saveData()
            R.id.toNewActivity -> {
                val intentNewActivity = Intent(this, SomeActivity::class.java)
                startActivity(intentNewActivity)
            }
        }
        return true
    }

    fun saveData(){
        val sharedPreferences = getSharedPreferences(Constants.ALL_DATA, MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putString(Constants.SURNAME, surname.getText().toString())
            putString(Constants.NAME, name.getText().toString())
            putString(Constants.MIDDLE_NAME, middleName.getText().toString())
            putString(Constants.AGE, age.getText().toString())
            putString(Constants.HOBBIE, hobbie.getText().toString())
        }.apply()

        Toast.makeText(this,R.string.saveDataMessage, Toast.LENGTH_SHORT).show()
    }

    fun onClickLoadData(view: View){
        val sharedPreferences = getSharedPreferences(Constants.ALL_DATA, MODE_PRIVATE)
        name.setText(sharedPreferences.getString(Constants.NAME, ""))
        surname.setText(sharedPreferences.getString(Constants.SURNAME, ""))
        middleName.setText(sharedPreferences.getString(Constants.MIDDLE_NAME, ""))
        age.setText(sharedPreferences.getString(Constants.AGE, ""))
        hobbie.setText(sharedPreferences.getString(Constants.HOBBIE, ""))
    }

    fun random(): Int{
        return (1..5).random()
    }

    fun changeBackground(int: Int){
        when (int){
            1 -> layoutMain.setBackgroundResource(R.drawable.with_gosts)
            2 -> layoutMain.setBackgroundResource(R.drawable.with_cats)
            3 -> layoutMain.setBackgroundResource(R.drawable.with_sw)
            4 -> layoutMain.setBackgroundResource(R.drawable.with_gp)
            5 -> layoutMain.setBackgroundColor(Color.rgb(255, 255, 255))
        }
    }
}